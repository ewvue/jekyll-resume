## 　6.IP隧道
在一个网络环境中，假如网络A、B使用IPv6，中间位置的网络C支持使用IPv4的话，网络A与网络B之间无法直接进行通信。为了让他们之间正常通信，这时需要采用IP隧道的功能。
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/9.png)

IP隧道中可以将那些从网络A发过来的IPv6的包统和为一个数据，再为之追加一个IPv4的首部以后转发给网络C，这种在网络层的首部后面继续追加网络层首部的通信方法就叫做“IP隧道”。
![](![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/10.png)
## TCP
- TCP作为一种面向有连接的协议，只有在确认通信对端存在时才会发送数据，从而可以控制通信流量的浪费。

- 为了通过IP数据报实现可靠性传输，需要考虑很多事情，例如：数据的破坏、丢包、重复以及分片顺序混乱等问题。

- TCP通过检验和、序列号、确认应答、重发控制、连接管理以及窗口控制等机制实现可靠性传输。

- 使用TCP的一个连接的建立与断开，正常过程下至少需要来回发送7个包才能完成，也就是我们常常听到的三次握手，两次挥手。
## 网络的构成要素
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/12.png)
- 网卡：
计算机连接网络时,必须要使用网卡,也被称作网络适配器、LAN卡。
- 中继器
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/13.png)
- 网桥
位于OSI模型中的第二层--数据链路层上连接两个网络的设备。
## 现代网络实态
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/14.png)
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/15.png)