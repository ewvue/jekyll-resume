# markdown
### 用途：
##### Markdown的语法简洁明了、学习容易，而且功能比纯文本更强，因此有很多人用它写博客。世界上最流行的博客平台WordPress和大型CMS如Joomla、Drupal都能很好的支持Markdown。完全采用Markdown编辑器的博客平台有Ghost和Typecho。用于编写说明文档，并且以"README.MD"的文件名保存在软件的目录下面。除此之外，现在由于我们有了RStudio这样的神级编辑器，我们还可以快速将Markdown转化为演讲PPT、Word产品文档、LaTex论文甚至是用非常少量的代码完成最小可用原型。在数据科学领域，Markdown已经广泛使用，极大地推进了动态可重复性研究的历史进程。
### 意义：
##### Markdown是一种不依赖特定工具、特定平台的标记方法。你可以随手打开一个notepad来写，也可以在任何支持文本输入的网页上写。
[markdown](https://gitee.com/ewvue/jekyll-resume/blob/master/2.7%E6%8C%81%E4%B9%85%E8%BF%9E%E6%8E%A5%E8%8A%82%E7%9C%81%E4%BF%A1%E9%80%9A.md)
